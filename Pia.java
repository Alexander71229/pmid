public class Pia{
	public double e;
	public double n;
	public double b;
	public int ni;
	public int nf;
	public Pos xi;
	public Pos xf;
	class Pos{
		public double p;
		public double t;
		public double o;
		public Pos(){
		}
		public Pos(double p,double t,double o){
			this.p=p;
			this.t=t;
			this.o=o;
		}
	}
	public Pia(){
		this(20,83,163,167,285);
	}
	public Pia(int ni,int nf){
		this(ni,nf,163,167,285);
	}
	public Pia(int ni,int nf,double e,double n,double b){
		this.ni=ni;
		this.nf=nf;
		this.e=e;
		this.n=n;
		this.b=b;
		this.xi=this.pos(ni);
		this.xf=this.pos(nf);
	}
	public Pos pos(int o){
    Pos res=new Pos();
    int p=o%12;
    double u=0;
		int t=0;
    if(p<=4){
      if(p%2==0){
      	t=p/2;
				res.o=0;
      }else{
      	u=3.*b/2.-(2.*n+e)/2.;
      	t=(p-1)/2;
				res.o=1;
      }
    }else{
      if(p%2==1){
      	t=(p+1)/2;
				res.o=0;
      }else{
      	u=5.*b-(3.*n+2.*e)/2.;
      	t=(p-6)/2;
				res.o=1;
      }
    }
    if(res.o==0){
			res.p=b*t;
     	res.t=b;
    }else{
    	res.p=(n+e)*t+u;
     	res.t=n;
    }
    res.p=res.p+(o-p)*(7*b)/12;
    return res;
	}
	public Pos posr(int o){
		Pos res=this.pos(o);
		/*if(res.o==0){
			Pos p2=this.pos(o+1);
			res.t=p2.p-res.p;
		}*/
		res.p=(res.p-this.xi.p)/(this.xf.p-this.xi.p);
		res.t=res.t/(this.xf.p-this.xi.p);
		return res;
  }
}