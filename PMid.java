import java.util.*;
import java.io.*;
import javax.sound.midi.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class PMid{
	public static String formato(int i){
		if(i<10){
			return "0"+i;
		}
		return i+"";
	}
	public static void dib(BufferedImage bi,Pia pia,int ni,int nf,int wpan,int hpan,int div)throws Exception{
		Graphics g=bi.getGraphics();
		double wd=wpan/div;
		for(int i=0;i<div;i++){
			for(int j=ni;j<=nf;j++){
				Pia.Pos ps=pia.posr(j);
				if(ps.o==0){
					g.setColor(new Color(255,255,255));
					g.fillRect((int)Math.round((wd)*i+ps.p*wd),0,(int)Math.round(ps.t*wd),hpan);
				}
			}
		}
		for(int i=0;i<div;i++){
			for(int j=ni;j<=nf;j++){
				Pia.Pos ps=pia.posr(j);
				if(ps.o==1){
					g.setColor(new Color(230,230,230));
					g.fillRect((int)Math.round((wd)*i+ps.p*wd),0,(int)Math.round(ps.t*wd),hpan);
				}
			}
		}
		for(int i=0;i<div;i++){
			g.setColor(new Color(0,0,0));
			g.drawRect((int)Math.round((wd)*i),0,(int)Math.round(wd),hpan);
		}
	}
	public static void main(String[]argumentos){
		try{
			HashMap<Integer,Integer>canales=new HashMap<Integer,Integer>();
			String nom="TTBB KYRIE_cuan";
			int div=4;
			int nns=16;
			canales.put(0,1);
			/*canales.put(2,1);
			canales.put(3,1);
			canales.put(4,1);*/
			int of=3;
			int ni=37;
			int nf=83;
			int wpan=1366;
			int hpan=768;
			double wd=wpan/div;
			Color[]colorc=new Color[]{new Color(146,234,157),new Color(173,210,156),new Color(123,189,223),new Color(214,124,236),new Color(114,245,213)};
			ArrayList<BufferedImage>bis=new ArrayList<BufferedImage>();
			Pia pia=new Pia(ni,nf);
			Sequence s=MidiSystem.getSequence(new File(nom+".mid"));
			int res=s.getResolution();
			HashMap<String,Long>h=new HashMap<String,Long>();
			Track[]ts=s.getTracks();
			for(int i=0;i<ts.length;i++){
				Track t=ts[i];
				for(int j=0;j<t.size();j++){
					MidiEvent me=t.get(j);
					long tk=me.getTick();
					try{
						ShortMessage sm=(ShortMessage)me.getMessage();
						if(canales.get(i)==1){
							if(sm.getCommand()==ShortMessage.NOTE_ON&&sm.getData2()>0){
								String k=sm.getData1()+":"+sm.getChannel();
								h.put(k,tk);
							}
							if((sm.getCommand()==ShortMessage.NOTE_OFF)||(sm.getCommand()==ShortMessage.NOTE_ON&&sm.getData2()==0)){
								String k=sm.getData1()+":"+sm.getChannel();
								if(h.get(k)!=null){
									long tf=h.get(k);
									tk=tk-of*res;
									tf=tf-of*res;
									long rs1=tf%(nns*res);
									long np1=(tf-rs1)/(nns*res);
									long rs2=tk%(nns*res);
									long np2=(tk-rs2)/(nns*res);
									double y1=(rs1*1.)/(nns*res);
									double y2=(rs2*1.)/(nns*res);
									Pia.Pos ps=pia.posr(sm.getData1());
									if(ps.o==0){
										ps.t=pia.posr(sm.getData1()+1).p-ps.p;
									}
									int k1=(int)Math.floor(np1/div);
									int k2=(int)Math.floor(np2/div);
									while(k1>=bis.size()||k2>=bis.size()){
										BufferedImage bi=new BufferedImage(wpan,hpan,BufferedImage.TYPE_INT_RGB);
										dib(bi,pia,ni,nf,wpan,hpan,div);
										bis.add(bi);
									}
									Graphics g=bis.get(k1).getGraphics();
									if(np2==np1){
										np1=np1%div;
										g.setColor(colorc[i]);
										g.fillRect((int)Math.round((wd)*np1+ps.p*wd),(int)Math.round(hpan-y2*hpan),(int)Math.round(ps.t*wd),(int)Math.round((rs2-rs1)*hpan/(1.*nns*res)));
										g.setColor(new Color(0,0,0));
										g.drawLine((int)Math.round((wd)*np1+ps.p*wd),(int)Math.round(hpan-y1*hpan),(int)Math.round((wd)*np1+ps.p*wd+ps.t*wd),(int)Math.round(hpan-y1*hpan));
									}else{
										np1=np1%div;
										np2=np2%div;
										g.setColor(colorc[i]);
										g.fillRect((int)Math.round((wd)*np1+ps.p*wd),0,(int)Math.round(ps.t*wd),(int)Math.round(hpan-y1*hpan));
										g.setColor(new Color(0,0,0));
										g.drawLine((int)Math.round((wd)*np1+ps.p*wd),(int)Math.round(hpan-y1*hpan),(int)Math.round((wd)*np1+ps.p*wd+ps.t*wd),(int)Math.round(hpan-y1*hpan));
										g=bis.get(k2).getGraphics();
										g.setColor(colorc[i]);
										g.fillRect((int)Math.round((wd)*np2+ps.p*wd),(int)Math.round(hpan-y2*hpan),(int)Math.round(ps.t*wd),(int)Math.round(y2*hpan));
									}
								}
							}
						}
					}catch(Exception e){
					}
				}
			}
			for(int i=0;i<bis.size();i++){
				ImageIO.write(bis.get(i),"png",new File(nom+formato(i)+".png"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}